#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <opencv2/opencv.hpp>
#include <ros/ros.h>

int main(int argc,
         char** argv)
{
  ros::init(argc, argv, "image_publisher");
  ros::NodeHandle nh;

  // Alrady set in the launch file. Cannot be set after ImageTransport object init
  // std::vector<std::string> disabled{"image_transport/compressedDepth", "image_transport/theora"};
  // nh.setParam("/camera/disable_pub_plugins", disabled);
  // nh.setParam("camera/compressed/jpeg_quality", 50);

  image_transport::ImageTransport it(nh);
  image_transport::Publisher pub = it.advertise("camera", 1);

  cv::VideoCapture cap;
  if (!cap.open(0))
    return 1;

  ros::Rate loop_rate(25);
  while (nh.ok())
  {
    cv::Mat frame;
    cap >> frame;
    if (frame.empty())
      break;

    sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
    pub.publish(msg);
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
