[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Compiling
## rosdep
Install, initialize and update [rosdep](http://wiki.ros.org/rosdep).

## Create a catkin workspace and clone the project:
```bash
mkdir -p $HOME/code/catkin_workspace/src
cd $HOME/code/catkin_workspace/src
git clone https://gitlab.com/InstitutMaupertuis/image_transport_test.git
cd ..
```

## Resolve ROS dependencies
```bash
rosdep install --from-paths src --ignore-src --rosdistro melodic -y
```

## Compile and install
Use `catkin_make`:
```bash
catkin_make
catkin_make install
```

Or `catkin tools`:
```bash
catkin config --install
catkin build
```

# How to launch
```bash
source devel/setup.bash
```

```bash
roslaunch image_transport_test camera.launch
```

Close RViz after a few seconds and inspect the `bag` file.

